from django.conf.urls import include, url
from django.contrib import admin
from apitest.urls import router
from rest_framework.authtoken import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^apitest/', include(router.urls)),
    url(r'^api-get-token/', views.obtain_auth_token),
]
