from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.permissions import IsAdminUser
from rest_framework.decorators import permission_classes

class UserSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'id', 'username', 'password', 'email', 'is_staff')
		extra_kwargs = {'password': {'write_only': True}}

	@permission_classes((IsAdminUser, ))
	def create(self, validated_data):
		user = User(username=validated_data['username'])
		user.set_password(validated_data['password'])
		user.save()
		return user		
